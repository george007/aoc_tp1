package metronome;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import metronome.controller.Controller;
import metronome.controller.IController;
import metronome.view.ihm.IHM;

public class JFXMain extends Application {

	private IController controller;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Load root layout from fxml file.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(JFXMain.class.getResource("view/metronomeView.fxml"));
		Parent root = loader.load();

		primaryStage.setTitle("Metronome");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();

		IHM ihm = loader.getController();
		controller = new Controller(ihm);
	}
	
	@Override
	public void stop() {
		//TODO Notify Controller to notify Engine to notify Clock to clear Timer... o.O
		controller.quit();
		Platform.exit();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
