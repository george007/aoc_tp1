package metronome.engine;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javafx.application.Platform;
import metronome.command.Command;

public class ClockImpl implements Clock {
	//List of active commands the Clock is periodically executing:
	HashMap<Command, TimerTask> commands;
	//Timer instance to manage all repetitive tasks:
	Timer timer;

	public ClockImpl() {
		commands = new HashMap<Command, TimerTask>();
		timer = new Timer(true);
	}
	
	@Override
	public void finalize() {
		//Cancel Timer's thread on finalizing Clock instance:
		timer.cancel();
	}

	@Override
	public void activatePeriodically(final Command cmd, float period) {
		long p = (long) (period * 1000);
		TimerTask tt = new TimerTask() {
			public void run() {
				Logger.getGlobal().info("Timer runs command: " + cmd.toString());
				//This way the command will be executed in Java FX UI thread: (https://stackoverflow.com/questions/16128423/how-to-update-the-label-box-every-2-seconds-in-java-fx)
				Platform.runLater(cmd::execute);
			}
		};
		timer.scheduleAtFixedRate(tt, p,p);
		commands.put(cmd, tt);
	}

	/**
	 * @see Clock#activateAfter(metronome.command.Command,float)
	 */
	@Override
	public void activateAfter(final Command cmd, float time) {
		timer.schedule(new TimerTask() {
			public void run() {
				Logger.getGlobal().info("Running with delay scheduled command: " + cmd.toString());
				//This way the command will be executed in Java FX UI thread: (https://stackoverflow.com/questions/16128423/how-to-update-the-label-box-every-2-seconds-in-java-fx)
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						cmd.execute();
					}
				});
			}
		}, (long) (time*1000));
	}

	/**
	 * @see Clock#desactivate(metronome.command.Command)
	 */
	@Override
	public void desactivate(Command cmd) {
		if ( commands.containsKey(cmd) ) {
			commands.get(cmd).cancel();
			Logger.getGlobal().info("Timer was asked to stop command " + cmd.toString()
			+ ". Purge value after :"+timer.purge());
		} else {
			Logger.getGlobal().warning("Timer is asked to stop command " + cmd.toString()
			+ ", but it can't be found.");
		}
	}

}
