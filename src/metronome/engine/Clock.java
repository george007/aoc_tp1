package metronome.engine;

import metronome.command.Command;

public interface Clock {
	/**
	 * Executes a command periodically.
	 * @param cmd Command to be executed
	 * @param period Miliseconds between each execution
	 */
	void activatePeriodically(Command cmd, float period);
	/**
	 * Executes a command once after defined number of miliseconds.
	 * @param cmd Command to be executed
	 * @param time Miliseconds after which timer should call cmd
	 */
	void activateAfter(Command cmd, float time);
	/**
	 * Stops executing a command that was previously scheduled to run periodically.
	 * @param cmd
	 */
	void desactivate(Command cmd);
}

