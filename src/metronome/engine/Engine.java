package metronome.engine;

import metronome.command.Command;

public interface Engine {
	
	public enum EvtEngine {Ongoing,MarkTempo,MarkMeasure,UpdateTempo,UpdateMeasure};
	
	int getTempo();
	void setTempo(int t);
	
	boolean getOnGoing();
	void setOnGoing(boolean b);
	
	int getMeasure();
	void setMeasure(int m);
	
	void setClock(Clock c);
	void setCmd(Command cmd, EvtEngine evt);
}