package metronome.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import metronome.command.Command;

public class EngineImpl implements Engine {

	private boolean onGoing;
	private int tempo;
	private int measure;

	private Clock clock;

	private Map<EvtEngine, Command> commands;

	public EngineImpl() {
		this.commands = new HashMap<EvtEngine, Command>();
		this.onGoing = false;
		this.tempo = 120;
		this.measure = 4;
	}

	@Override
	public int getTempo() {
		return tempo;
	}

	@Override
	public void setTempo(int t) {
		if (t <= 0) return;
		
		tempo = t;
		if (onGoing) {
			Logger.getGlobal().info("Changed Tempo in clock");
			this.setOnGoing(false);
			this.setOnGoing(true);
		}
		
		commands.get(EvtEngine.UpdateTempo).execute();
	}

	@Override
	public boolean getOnGoing() {
		return onGoing;
	}

	@Override
	public void setOnGoing(boolean b) {
		if (b == onGoing) return;
		if (b) {
			clock.activatePeriodically(commands.get(EvtEngine.MarkTempo), (60f / tempo));
			clock.activatePeriodically(this.commands.get(EvtEngine.MarkMeasure), (60f / tempo * measure));
		} else {
			clock.desactivate(commands.get(EvtEngine.MarkTempo));
			clock.desactivate(commands.get(EvtEngine.MarkMeasure));
		}
		onGoing = b;
	}

	@Override
	public int getMeasure() {
		return measure;
	}

	@Override
	public void setMeasure(int m) {
		Logger.getGlobal().info("Engine changes Measure to: " + Integer.toString(m));
		if (m > 1 && m < 8) {
			measure = m;
			//Restart all to keep Tempo and Measure synced: 
			this.setOnGoing(false);
			this.setOnGoing(true);
		}
	}

	@Override
	public void setCmd(Command cmd, EvtEngine evt) {
		this.commands.put(evt, cmd);
	}

	@Override
	public void setClock(Clock c) {
		this.clock = c;
	}
}
