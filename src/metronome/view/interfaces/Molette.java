package metronome.view.interfaces;

import javafx.scene.control.Slider;
import metronome.command.Command;

/**
 * @author Piotr Grzegorzewski & Yvanne Chaussin
 */
public interface Molette {
    void setMolette(Slider slider);
    float position();
    void onChange(Command cmd);
}