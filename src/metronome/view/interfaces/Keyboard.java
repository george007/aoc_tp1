package metronome.view.interfaces;

import javafx.scene.control.Button;

import java.util.Map;

/**
 * @author Piotr Grzegorzewski & Yvanne Chaussin
 */
public interface Keyboard {

    int START_BUTTON = 0;
    int STOP_BUTTON = 1;
    int INC_BUTTON = 2;
    int DEC_BUTTON = 3;

    /**
     * Detects if a key (defined by <code>i</code>) is realeased or pressed.
     * @param i Key code
     * @return Return true if the key <code>i</code> is pressed, else false.
     */
    boolean keyPressed(int i);

    void setButtons(Map<Integer, Button> buttons);
}
