package metronome.view.interfaces;

/**
 * @author Piotr Grzegorzewski & Yvanne Chaussin
 */
public interface SoundEmitter {
    void beep();
}
