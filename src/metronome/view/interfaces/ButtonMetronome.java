package metronome.view.interfaces;

import metronome.command.Command;

public interface ButtonMetronome {
    void click(Command cmd);
}
