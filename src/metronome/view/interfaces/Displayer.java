package metronome.view.interfaces;

import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

/**
 *
 * @author Piotr Grzegorzewski & Yvanne Chaussin
 */
public interface Displayer {
    int LED_TEMPO = 0;
    int LED_MEASURE = 1;

    void setLedTempo(Circle led);
    void setLedMeasure(Circle led);
    void setScreen(Label screen);

    // Manage LEDs
    void turnOnLED(int numLed);
    void turnOffLED(int numLed);

    // Display an integer on the external screen of the metronome
    void displayTempo(int valueTempo);
}
