package metronome.view.adapter;

import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import metronome.view.interfaces.Displayer;

import java.util.logging.Logger;

public class DisplayerImpl implements Displayer {

    private Circle tempoLED;
    private Circle measureLED;
    private Label display;

    public DisplayerImpl() {
        super();
    }

    @Override
    public void setLedTempo(Circle led) {
        this.tempoLED = led;
    }

    @Override
    public void setLedMeasure(Circle led) {
        this.measureLED = led;
    }

    @Override
    public void setScreen(Label screen) {
        this.display = screen;
    }

    @Override
    public void turnOnLED(int numLed) {
        switch (numLed) {
            case LED_TEMPO:
                tempoLED.getStyleClass().remove("inactiveLED");
                tempoLED.getStyleClass().add("activeLED");
                break;
            case LED_MEASURE:
                measureLED.getStyleClass().remove("inactiveLED");
                measureLED.getStyleClass().add("activeLED");
                break;
            default:
                Logger.getGlobal().warning("Can not find the LED : " + numLed);
                break;
        }
    }

    @Override
    public void turnOffLED(int numLed) {
        switch (numLed) {
            case LED_TEMPO:
                tempoLED.getStyleClass().remove("activeLED");
                tempoLED.getStyleClass().add("inactiveLED");
                break;
            case LED_MEASURE:
                measureLED.getStyleClass().remove("activeLED");
                measureLED.getStyleClass().add("inactiveLED");
                break;
            default:
                Logger.getGlobal().warning("Can not find the LED : " + numLed);
        }

    }

    @Override
    public void displayTempo(int valueTempo) {
        display.setText(Integer.toString(valueTempo));
    }
}
