package metronome.view.adapter;

import metronome.command.Command;
import metronome.view.Hardware;
import metronome.view.interfaces.ButtonMetronome;

public class ButtonAdapter implements ButtonMetronome {

    private int button;
    private Command onClickCommand;
    private boolean current;

    public ButtonAdapter(int btn) {
        this.button = btn;
        this.current = false;
        Hardware.getClock().activatePeriodically(this::poll, 0.01f);
    }

    @Override
    public void click(Command cmd) {
    	this.onClickCommand = cmd;
    }

    private void poll() {
        boolean pressed = Hardware.getKeyboard().keyPressed(button);
        if (pressed && !current && onClickCommand != null) {
            onClickCommand.execute();
        }
        current = pressed;
    }
}
