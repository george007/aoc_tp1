package metronome.view.adapter;

import javafx.scene.control.Slider;
import metronome.command.Command;
import metronome.view.Hardware;
import metronome.view.interfaces.Molette;

public class MoletteAdapter implements Molette {

    private int currentTempo;
    private Slider slider;
    private Command onChangeCmd;

    public MoletteAdapter() {
        currentTempo = 0;
        Hardware.getClock().activatePeriodically(this::poll, 0.1f);
    }

    @Override
    public void setMolette(Slider slider) {
        this.slider = slider;
    }

    @Override
    public float position() {
        return (float) slider.getValue();
    }

    @Override
    public void onChange(Command cmd) {
        this.onChangeCmd = cmd;
    }

    private void poll() {
        int newTempo = (int) this.position();
        if (currentTempo != newTempo && onChangeCmd != null) {
            this.onChangeCmd.execute();
        }
    }
}
