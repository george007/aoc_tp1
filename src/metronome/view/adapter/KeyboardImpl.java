package metronome.view.adapter;

import javafx.scene.control.Button;
import metronome.view.interfaces.Keyboard;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class KeyboardImpl implements Keyboard {

    private Map<Integer, Button> buttons;

    public KeyboardImpl() {
        buttons = new HashMap<>();
    }

    @Override
    public boolean keyPressed(int i) {
        Button b = null;

        switch (i) {
            case START_BUTTON :
            case STOP_BUTTON:
            case INC_BUTTON:
            case DEC_BUTTON:
                b = buttons.get(i);
                break;
            default:
                Logger.getGlobal().warning("This button is not referenced.");
                break;
        }

        return b.isPressed();
    }

    @Override
    public void setButtons(Map<Integer, Button> buttons) {
        this.buttons.putAll(buttons);
    }
}
