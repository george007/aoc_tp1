package metronome.view.adapter;

import metronome.view.interfaces.SoundEmitter;

import java.awt.*;

public class SoundEmitterImpl implements SoundEmitter {

    //Beep
    private Toolkit tk;

    public SoundEmitterImpl() {
        this.tk = Toolkit.getDefaultToolkit();
    }

    @Override
    public void beep() {
        tk.beep(); //If beep is not heard on Ubuntu: sudo modprobe pcspkr
    }
}
