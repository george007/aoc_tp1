package metronome.view.ihm;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;
import metronome.view.Hardware;
import metronome.view.adapter.ButtonAdapter;
import metronome.view.interfaces.*;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


public class IHM implements Initializable {

    //Elements of the view:
    @FXML
    private Slider slider;
    @FXML
    private Circle tempoLED;
    @FXML
    private Circle measureLED;
    @FXML
    private Label display;
    @FXML
    private Button startBtn;
    @FXML
    private Button stopBtn;
    @FXML
    private Button incBtn;
    @FXML
    private Button decBtn;

    private ButtonMetronome startButton;
    private ButtonMetronome stopButton;
    private ButtonMetronome incButton;
    private ButtonMetronome decButton;

    public IHM() {
        super();
    }

    public ButtonMetronome getStartButton() {
        return startButton;
    }

    public ButtonMetronome getStopButton() {
        return stopButton;
    }

    public ButtonMetronome getIncButton() {
        return incButton;
    }

    public ButtonMetronome getDecButton() {
        return decButton;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Hardware.getMolette().setMolette(slider);

        Hardware.getDisplayer().setLedTempo(tempoLED);
        Hardware.getDisplayer().setLedMeasure(measureLED);
        Hardware.getDisplayer().setScreen(display);

        this.startButton = new ButtonAdapter(Keyboard.START_BUTTON);
        this.stopButton = new ButtonAdapter(Keyboard.STOP_BUTTON);
        this.incButton = new ButtonAdapter(Keyboard.INC_BUTTON);
        this.decButton = new ButtonAdapter(Keyboard.DEC_BUTTON);

        Map<Integer,Button> buttons = new HashMap<>();
        buttons.put(Keyboard.START_BUTTON, startBtn);
        buttons.put(Keyboard.STOP_BUTTON, stopBtn);
        buttons.put(Keyboard.INC_BUTTON, incBtn);
        buttons.put(Keyboard.DEC_BUTTON, decBtn);
        Hardware.getKeyboard().setButtons(buttons);
    }
}
