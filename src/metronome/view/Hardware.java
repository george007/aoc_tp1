package metronome.view;

import metronome.engine.Clock;
import metronome.engine.ClockImpl;
import metronome.view.adapter.DisplayerImpl;
import metronome.view.adapter.KeyboardImpl;
import metronome.view.adapter.MoletteAdapter;
import metronome.view.adapter.SoundEmitterImpl;
import metronome.view.interfaces.*;

/**
 * @author Piotr Grzegorzewski & Yvanne Chaussin
 */
public class Hardware {

    private static Clock clock = new ClockImpl();
    private static Keyboard keyboard = new KeyboardImpl();
    private static  Molette molette = new MoletteAdapter();
    private static  Displayer displayer = new DisplayerImpl();
    private static  SoundEmitter soundEmitter = new SoundEmitterImpl();

    public static Clock getClock() {
        return clock;
    }

    public static Keyboard getKeyboard() {
        return keyboard;
    }

    public static Molette getMolette() {
        return molette;
    }

    public static SoundEmitter getSoundEmitter() {
        return soundEmitter;
    }

    public static Displayer getDisplayer() {
        return displayer;
    }
}
