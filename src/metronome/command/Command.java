package metronome.command;

/**
 *
 */
public interface Command {
	void execute();
}
