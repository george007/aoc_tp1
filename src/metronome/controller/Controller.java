package metronome.controller;

import java.util.logging.Logger;

import metronome.engine.Engine;
import metronome.engine.EngineImpl;
import metronome.engine.Engine.EvtEngine;
import metronome.view.Hardware;
import metronome.view.interfaces.*;
import metronome.view.ihm.IHM;

public class Controller implements IController {
	
	//Engine
	private Engine engine;

	/**
	 * Initialize a controller and define all of the different
	 */
	public Controller(IHM ihm) {
		this.engine = new EngineImpl();

		// Engine Initialization
		this.engine.setClock(Hardware.getClock());

        // Command Initialization
		this.engine.setCmd(this::markTempo, EvtEngine.MarkTempo);
		this.engine.setCmd(this::markMeasure, EvtEngine.MarkMeasure);
		this.engine.setCmd(() -> Hardware.getDisplayer().displayTempo(engine.getTempo()), EvtEngine.UpdateTempo);

		ihm.getStartButton().click(this::start);
		ihm.getStopButton().click(this::stop);
		ihm.getIncButton().click(this::increaseMeasure);
		ihm.getDecButton().click(this::decreaseMeasure);

		Hardware.getMolette().onChange(this::setMolette);
	}

	////////////////////////
	// IController methods:
	////////////////////////
	@Override
	public void start() {
		if (!engine.getOnGoing()) {
			engine.setOnGoing(true);
			status("Engine is starting.");
		} else {
			status("Engine is already running.");
		}
	}
	
	@Override
	public void stop() {
		status("Stop button clicked");
		if (engine.getOnGoing() ) {
			engine.setOnGoing(false);
		} else {
			status("Engine is already stopped.");
		}
	}

    @Override
    public void increaseMeasure() {
        if (engine.getOnGoing()) {
            this.engine.setMeasure(this.engine.getMeasure() + 1);
        }
    }

    @Override
    public void decreaseMeasure() {
        if (engine.getOnGoing()) {
            this.engine.setMeasure(this.engine.getMeasure() - 1);
        }
    }

    @Override
	public void quit() {
		status("Application is closed.");
		if (engine.getOnGoing()) {
			engine.setOnGoing(false);
		}
	}
	
	/**
	 * Blinks LED1 to mark the tempo
	 */
	@Override
	public void markTempo() {
		Hardware.getDisplayer().turnOnLED(Displayer.LED_TEMPO);
		Hardware.getSoundEmitter().beep();
		Hardware.getClock().activateAfter(() -> {
			Hardware.getDisplayer().turnOffLED(Displayer.LED_TEMPO);
		}, 0.1f);
	}
	
	/**
	 * Blinks LED2 to mark measure
	 */
	@Override
	public void markMeasure() {
		Hardware.getDisplayer().turnOnLED(Displayer.LED_MEASURE);
		Hardware.getClock().activateAfter(() -> {
			Hardware.getDisplayer().turnOffLED(Displayer.LED_MEASURE);
		}, 0.1f);
	}

	public void setMolette() {
		int currentTempo = engine.getTempo();
		int newTempo = (int) Hardware.getMolette().position();
		if (currentTempo != newTempo) {
			engine.setTempo(newTempo);
		}
	}
	
	private void status(String s) {
		Logger.getGlobal().info(s);
	}
}
