/**
 * 
 */
package metronome.controller;

public interface IController {
	
	/**
	 * 
	 */
	void start();
	
	/**
	 * 
	 */
	void stop();
	
	void markTempo();
	
	void markMeasure();

	void increaseMeasure();

	void decreaseMeasure();

	void quit();
}
